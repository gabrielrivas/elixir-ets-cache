defmodule Cache.Supervisor do
  use Supervisor

  def start_link do
    # We are now registering our supervisor process with a name
    # so we can reference it in the `create_table/1` function
    Supervisor.start_link(__MODULE__, [], name: :cache_supervisor)
  end

  def init(_) do
    children = [
      worker(Cache.Server, [])
    ]

    # We also changed the `strategty` to `simple_one_for_one`.
    # With this strategy, we define just a "template" for a child,
    # no process is started during the Supervisor initialization,
    # just when we call `start_child/2`
    supervise(children, strategy: :simple_one_for_one)
  end
end
