defmodule Cache do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    #link_opts = [strategy: :one_for_one, name: TelLog.LinkSupervisor]

    children = [
      supervisor(Cache.Supervisor, [])
    ]

    # start our links like so?
    #Supervisor.start_child(:link_supervisor, [{"ztopic", "jbus"}])

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Cache.Supervisor]
    Supervisor.start_link(children, opts)
  end

  #table_name : atom
  #Create a new supervised child task under :cache_supervisor  
  def create(table_name) do
    Supervisor.start_child(:cache_supervisor, [table_name])
  end

  #Deletes the ets table  
  def delete(table_name) do
    Cache.Server.delete(table_name)
  end

  def set(table_name, key, value) do
    Cache.Server.set(table_name, key, value)
  end

  def remove(table_name, key) do
    Cache.Server.remove(table_name, key)
  end

  def get(table_name, key) do
    Cache.Server.get(table_name, key)
  end

  def get_all(table_name) do
    Cache.Server.get_all(table_name)
  end 
end
