defmodule Cache.Server do
  use GenServer
  # API

  #GenServer.start_link(arg1, arg2, arg3)
  #arg1 : Name of the module implementing the GenServer
  #arg2 : Parameters passed to the module init function
  #arg3 : Name registration for this GenServer
  #First function to be called when this GenServer is linked to
  #its parent process (can also be a supervisor)
  def start_link(name) do
    GenServer.start_link(__MODULE__, name, name: via_tuple(name))
  end

  defp via_tuple(table_name) do
    {:via, :gproc, {:n, :l, {:table, table_name}}}
  end

  def set(table_name, key, value) do
    # And the `GenServer` callbacks will accept this tuple the
    # same way it accepts a pid or an atom.
    GenServer.cast(via_tuple(table_name), {:set, key, value})
  end

  def remove(table_name, key) do
    GenServer.cast(via_tuple(table_name), {:remove, key})
  end

  def delete(table_name) do
    GenServer.cast(via_tuple(table_name), {:delete})
  end

  def get(table_name, key) do
    GenServer.call(via_tuple(table_name), {:get,key})
  end

  def get_all(table_name) do
    GenServer.call(via_tuple(table_name), {:get_all})
  end

  def init(table) do
    {:ok, :ets.new(table , [])}
  end

  def handle_cast({:set, key, value}, table) do
    :ets.insert(table, {key, value})
    {:noreply, table}
  end
  
  def handle_cast({:remove, key}, table) do
    :ets.delete(table, key)
    {:noreply, table}
  end

  def handle_cast({:delete}, table) do
    :ets.delete(table)
    {:noreply, table}
  end

  def handle_call({:get, key}, _from, table) do
    case :ets.lookup(table, key) do
      [{^key, value}] -> {:reply, value, table}
      [] -> {:reply, false, table}
    end
  end

  def handle_call({:get_all}, _from, table) do
    IO.inspect table
    {:reply, :ets.tab2list(table), table}
  end  
end
